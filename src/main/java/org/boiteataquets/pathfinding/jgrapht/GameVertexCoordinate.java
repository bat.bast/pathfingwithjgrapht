package org.boiteataquets.pathfinding.jgrapht;

import lombok.Getter;

import java.util.Objects;

public class GameVertexCoordinate {
    @Getter
    private final int col;
    @Getter
    private final int row;

    public GameVertexCoordinate(int row, int col) {
        this.row = row;
        this.col = col;

    }

    @Override
    public String toString() {
        return "v_" + row + "_" + col;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }

        if (!(o instanceof GameVertexCoordinate)) {
            return false;
        }

        GameVertexCoordinate v = (GameVertexCoordinate) o;
        return row == v.getRow() && col == v.getCol();
    }
    @Override
    public int hashCode() {
        return Objects.hash(getRow(), getCol());
    }
}
