package org.boiteataquets.pathfinding.jgrapht;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.AStarShortestPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultUndirectedWeightedGraph;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.util.SupplierUtil;

import java.util.List;
import java.util.function.Supplier;

public class GameGraph {
    private final double[][] gameGrid;
    private final Graph<GameVertexCoordinate, DefaultWeightedEdge> gameGraph;

    public GameGraph(double[][] gameGrid) {
        // Store game matrix
        this.gameGrid = gameGrid;

        // vertice supplier
        // This code is called when the Graph Genarator build each vertex
        Supplier<GameVertexCoordinate> verticeSupplier = new Supplier<>() {
            private int id = 0;
            private int rows = gameGrid.length;
            private int cols = gameGrid[0].length;

            // This code is called by Graph Generator by method public void generateGraph(Graph<V, E> target, Map<String, V> resultMap)
            // It is used to name vertex
            @Override
            public GameVertexCoordinate get() {
                int row = id / cols;
                int col = id - row * cols;
                id++;
                // Coordinate start at 1, not 0
                return new GameVertexCoordinate(row + 1, col + 1);
            }
        };

        // Build Graph with edges not directed but with weight
        // After this line, Graph is still empty: it will be populated by Graph Generator
        gameGraph = new DefaultUndirectedWeightedGraph(verticeSupplier, SupplierUtil.createDefaultWeightedEdgeSupplier());

        // Use this specific Game Graph Generator to build vertices and edges
        GameGridGraphGenerator<GameVertexCoordinate, DefaultWeightedEdge> gameGridGenerator = new GameGridGraphGenerator<>(gameGrid);

        // Populate Game Graph with vertices and edges
        gameGridGenerator.generateGraph(gameGraph);
    }

    private List<GameVertexCoordinate> getShortestPath(ShortestPathAlgorithm shortestPathAlgo, GameVertexCoordinate source, GameVertexCoordinate sink) {
        GraphPath path = shortestPathAlgo.getPath(source, sink);
        if (path != null) {
            List<GameVertexCoordinate> shortestPath = path.getVertexList();
            return shortestPath;
        } else {
            // There is no path
            return null;
        }
    }

    public List<GameVertexCoordinate> getDijkstraShortestPath(GameVertexCoordinate source, GameVertexCoordinate sink) {
        DijkstraShortestPath shortestPathAlgo = new DijkstraShortestPath(gameGraph);
        return getShortestPath(shortestPathAlgo, source, sink);
    }

    public List<GameVertexCoordinate> getAStarShortestPath(GameVertexCoordinate source, GameVertexCoordinate sink) {
        // TODO AdmissibleHeuristic must not be null
        AStarShortestPath shortestPathAlgo = new AStarShortestPath(gameGraph, null);
        return getShortestPath(shortestPathAlgo, source, sink);
    }
}
