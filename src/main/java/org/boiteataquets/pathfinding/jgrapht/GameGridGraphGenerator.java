package org.boiteataquets.pathfinding.jgrapht;

import lombok.extern.slf4j.Slf4j;
import org.jgrapht.Graph;
import org.jgrapht.generate.GraphGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* Largement repris de GridGraphGenerator
La matrice est de la forme suivante : plus le chiffre augmente, plus le chemin sera évité
    0 --> Passage par ce sommet facile (un mur)
    100 --> passage par ce sommet difficile, avec un poids maximum (un chemin)
    entre 1 et 99 --> passage par ce sommet possible, avec un point intermédiaire (un cours d'eau)

     0 0 1 2 3
     0 1 2 3 4
     1 1 2 2 2
     0 0 0 0 0
     1 2 3 4 0
 */
@Slf4j
public class GameGridGraphGenerator<V, E> implements GraphGenerator<V, E, V> {
    private final double[][] gameGrid;

    public static final String CORNER_VERTEX = "Corner Vertex";

    private final int rows;
    private final int cols;

    public GameGridGraphGenerator(double[][] gameGrid) {
        rows = gameGrid.length;
        if (rows < 2) {
            throw new IllegalArgumentException(
                    "illegal number of rows (" + rows + "). there must be at least two.");
        }
        cols = gameGrid[0].length;
        if (cols < 2) {
            throw new IllegalArgumentException(
                    "illegal number of columns (" + cols + "). there must be at least two.");
        }
        this.gameGrid = gameGrid;
    }

    @Override
    public void generateGraph(Graph<V, E> target, Map<String, V> resultMap) {
        List<V> vertices = new ArrayList<>(rows * cols);

        // Adding all vertices to the set
        int cornerCtr = 0;
        for (int i = 0; i < rows * cols; i++) {
            V vertex = target.addVertex();
            log.debug("Add Vertex " + vertex);
            vertices.add(vertex);

            boolean isCorner = (i == 0) || (i == (cols - 1)) || (i == (cols * (rows - 1)))
                    || (i == ((rows * cols) - 1));
            if (isCorner && (resultMap != null)) {
                resultMap.put(CORNER_VERTEX + ' ' + ++cornerCtr, vertex);
            }
        }

        // Adding all edges
        for (int i = 1; i <= vertices.size(); i++) {
            for (int j = 1; j <= vertices.size(); j++) {
                // We add edges only on the right or below :
                //    - vertex at j is at one position of the vertex at i (the path can use rigth, left, up or down, not a diagonal)
                //          --> (i + 1) == j for the same row
                //          --> (i + cols) == j for the next row
                //    - vertex at i is not the last of the row (i % cols) > 0
                V vertexI = vertices.get(i - 1);
                V vertexJ = vertices.get(j - 1);
                log.debug("Vertice I " + vertexI + " - Vertice J " + vertexJ);
                if (((i % cols) > 0 && (i + 1) == j) || (i + cols) == j) {
                    log.debug("Add Edge " + vertexI + " - " + vertexJ);
                    E edge = target.addEdge(vertexI, vertexJ);
                    GameVertexCoordinate vJ = (GameVertexCoordinate) vertexJ;
                    target.setEdgeWeight(edge, gameGrid[vJ.getRow() - 1][vJ.getCol() - 1]);
                }
            }
        }
    }
}
