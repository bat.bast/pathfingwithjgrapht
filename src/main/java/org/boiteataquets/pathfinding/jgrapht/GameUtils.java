package org.boiteataquets.pathfinding.jgrapht;

public abstract class GameUtils {
    public static String prettyPrintMatrixDouble(double[][] matrix) {
        StringBuffer out = new StringBuffer();
        if (matrix != null && matrix instanceof double[][]) {
            for (double[] row : matrix) {
                for (double col : row) {
                    out.append(col).append("\t");
                }
                out.append("\n");
            }
        }
        return out.toString();
    }
}
