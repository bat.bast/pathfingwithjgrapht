package org.boiteataquets.pathfinding.jgrapht;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class Launcher {

    public static void main(String[] args) {
        /*
       0 0 0 0 0 0
       0 1 1 1 1 1
       0 0 0 0 0 0
       1 1 1 0 1 1
       0 0 0 0 0 0
        */
        final double[][] gameMatrix = {{0, 0, 0, 0, 0, 0},
                {0, 1, 1, 1, 1, 1},
                {0, 0, 0, 0, 0, 0},
                {1, 1, 1, 0, 1, 1},
                {0, 0, 0, 0, 0, 0}};

        log.info("Game Matrix = \n" + GameUtils.prettyPrintMatrixDouble(gameMatrix));

        GameGraph gameGraph = new GameGraph(gameMatrix);
        List<GameVertexCoordinate> shortestPathD =
                gameGraph.getDijkstraShortestPath(new GameVertexCoordinate(1, 1),
                        new GameVertexCoordinate(5, 6));
        log.info("Shortest Path = " + shortestPathD.toString());

        /*
        List<GameVertexCoordinate> shortestPathA =
                gameGraph.getAStarShortestPath(new GameVertexCoordinate(1, 1),
                        new GameVertexCoordinate(5, 3));
        log.info(shortestPathA.toString());
        */
    }
}
